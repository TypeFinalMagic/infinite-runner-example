﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

    HUD hud;
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            hud = GameObject.Find("Main Camera").GetComponent<HUD>();
            hud.ChangeScore(10);
            Destroy(this.gameObject);
        }
    }
}
