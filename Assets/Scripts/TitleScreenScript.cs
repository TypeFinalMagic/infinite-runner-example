﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleScreenScript : MonoBehaviour {

    /* Use to start the game */
    public void StartGame()
    {
        //Debug.Log("test");
        Application.LoadLevel(0);
    }

    public void QuitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        //Application.Quit();
    }
}
