﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

    int score = 0;

	void Start () 
    {
        score = PlayerPrefs.GetInt("Score");
	}

    void Update()
    {
        if(Input.GetButtonDown("Submit"))
        {
            Application.LoadLevel(0);
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 2 - 40, 50, 80, 30), "GAME OVER");
        GUI.Label(new Rect(Screen.width / 2 - 40, 300, 80, 30), "Score: " + score);
        GUI.Button(new Rect(Screen.width / 2 - 30, 350, 60, 30), "Retry");
        //if (GUI.Button(new Rect(Screen.width / 2 - 30, 350, 60, 30), "Retry"))
        //{
        //    Application.LoadLevel(0);
        //}
    }
}
