﻿using UnityEngine;
using System.Collections;

public class BackgroundScrolling : MonoBehaviour {

    public float speed = 0;
	
	// Update is called once per frame
	void Update () 
    {
        Scrolling();
	}

    void Scrolling()
    {
        renderer.material.mainTextureOffset = new Vector2((Time.time * speed) % 1, 0f);
    }
}
